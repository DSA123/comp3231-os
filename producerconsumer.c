/ This file will contain your solution. Modify it as you wish. /
#include <types.h>
#include "producerconsumer_driver.h"
#include <thread.h>
#include <synch.h>
#include <lib.h>


volatile int consPtr;
volatile int prodPtr;
struct semaphore *full;
struct semaphore *empty;
struct semaphore *consMutex;
struct semaphore *prodMutex;

struct pc_data buffer[BUFFER_SIZE];

/ This is called by a consumer to request more data. /
struct pc_data
consumer_consume(void)
{
	struct pc_data thedata;

	P(full);
	P(consMutex);

	thedata = buffer[consPtr];
	consPtr = (consPtr + 1) % 2;

	V(consMutex);
	V(empty);

	return thedata;
}

/ This is called by a producer to store data. /
void
producer_produce(struct pc_data item)
{

	P(empty);
	P(prodMutex);

	buffer[prodPtr] = item;

	prodPtr = (prodPtr + 1) % BUFFER_SIZE; 
	V(prodMutex);
	V(full);
}

/ Perform any initialisation (e.g. of global data) you need here /
void
producerconsumer_startup(void)
{
  	consPtr = 0;
 	prodPtr = 0;

	consMutex = sem_create("consMutex", 1);
	prodMutex = sem_create("prodMutex", 1);
	empty = sem_create("empty", BUFFER_SIZE);
	full = sem_create("full", 0);

	if (empty == NULL || full == NULL) {
		panic("maths: sem create failed");
	}
}

/ Perform any clean-up you need here /
void
producerconsumer_shutdown(void)
{
	sem_destroy(consMutex);
	sem_destroy(prodMutex);
	sem_destroy(empty);
	sem_destroy(full);
}
